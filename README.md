# Introduction

![GitBook logo](images/gitbook-logo.png)

[GitBook](https://gitbook.com) is a tool that converts Markdown documents into HTML or PDF. This has some advantages:

 * Markdown is [easy to write]((https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet), either manually or with a [WYSIWYG editor](https://www.gitbook.com/editor).
 	![GitBook Editor](images/gitbook-editor.png)
 * Markdown is easy to read -- the documentation source is readable even without building it into HTML. [View source](https://bitbucket.org/snyder13/gitbook-demo/src)
 * Storing the documentation source in git means vesioning is handled at a fine grain, and anyone who has a copy of the code also has documentation for that exact version.

