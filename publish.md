# Publishing

The book is built to the `_book` path, so publishing is a matter of moving that somewhere where people can get to it.

Typically this means pushing it to a separate git repository that is exposed to the web, either on your own server or using something like bitbucket/github.io.

There is a short script in `package.json` called `publish`, and if you edit the URL part of this to be a link to your own git repository instead of my bitbucket page, you can publish your work to that repo by running `npm run-script publish`.

