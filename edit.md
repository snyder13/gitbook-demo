# Editing the book

## Start the GitBook development server:

```bash
$ npm start

> @ start /home/git/gitbook-demo
> gitbook serve

Live reload server started on port: 35729
Press CTRL+C to quit ...

info: 7 plugins are installed 
info: loading plugin "livereload"... OK 
info: loading plugin "highlight"... OK 
info: loading plugin "search"... OK 
info: loading plugin "lunr"... OK 
info: loading plugin "sharing"... OK 
info: loading plugin "fontsettings"... OK 
info: loading plugin "theme-default"... OK 
info: found 2 pages 
info: found 7 asset files 
info: >> generation finished with success in 2.0s ! 

Starting server ...
Serving book on http://localhost:4000
```

Visit the given URL and you should see this documentation. When you make markdown changes the server will rebuild the site and refresh your browser. This is particularly useful if you opt not to use a WYSIWYG editor in order to verify that things look like you expected them to.

## Understand the GitBook layout

The main advantage of the editor is that it handles much of the structure of the book for you, but it's just a matter of following a few conventions:

 | Path                                                     | Usage                                                                                                                              |
 |:--------------------------------------------------------:| ---------------------------------------------------------------------------------------------------------------------------------- |
 | [book.json](https://toolchain.gitbook.com/config.html)   | This contains information about the book as a whole, like its title and authors. This is also where you can include more plugins.  |
 | README.md                                                | The "main" introductory page for the documentation                                                                                 |
 | SUMMARY.md                                               | The table of contents. All other .md files must be referenced here in order to be built during publishing.                         |
 | styles/\*.css                                            | This folder contains stylesheets for each possible output format.                                                                  |
 | [package.json](https://docs.npmjs.com/files/package.json)| You can use this to bring in other GitBook plugins on `npm install`, and to script interactions with the GitBook client.           |

## Write!

Title your book and put your name in `book.json`

To add a new page, create a new `.md` file for it and list it in `SUMMARY.md`

```markdown
 * [My chapter](chapter.md)
    * [My subsection](subsection.md)
 * [Another chapter](another.md)
```

(This also shows how you can link between your pages -- GitBook will convert the reference in parentheses to the correct HTML path)

Check out the [Markdown Cheat Sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) for everything else.
