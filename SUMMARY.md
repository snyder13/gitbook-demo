# Summary

* [Introduction](README.md)
* [Demo](demo.md)
	* [Installation](install.md)
	* [Editing the book](edit.md)
	* [Publishing](publish.md)
	* [Extending GitBook](extend.md)
