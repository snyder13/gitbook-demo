# Installation

## Install prerequisites

[node.js & npm](https://nodejs.org).

## Clone this repository

Ordinarily the documentation source would live in the same repo as the software it references.

```bash
$ git clone https://bitbucket.org/snyder13/gitbook-demo.git
```

## Install the demo package using npm

```bash
$ npm install

> undefined postinstall /home/git/gitbook-demo
> gitbook build

info: 7 plugins are installed 
info: 6 explicitly listed 
info: loading plugin "highlight"... OK 
info: loading plugin "search"... OK 
info: loading plugin "lunr"... OK 
info: loading plugin "sharing"... OK 
info: loading plugin "fontsettings"... OK 
info: loading plugin "theme-default"... OK 
info: found 2 pages 
info: found 6 asset files 
info: >> generation finished with success in 0.7s ! 
/home/git/gitbook-demo
└─┬ gitbook-cli@2.3.0 
		```

Running this installation also builds the first HTML version of the documentation. You can run `npm install` again at any time to regenerate the web site.

The gitbook client is installed in the relative path `node_modules/.bin/gitbook`. For convenience you might wish to `npm install -g gitbook` to install the utility on your path, so you can run it by simply typing `gitbook`.


